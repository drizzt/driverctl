# driverctl completion

_driverctl()
{
	local cur prev words cword
	_init_completion || return

	case $prev in
	     load-override|set-override|unset-override)
		COMPREPLY=($(compgen -W '$(${1:-driverctl} list-devices | cut -d" " -f1)' -- "$cur"))
		return 0
	     ;;
	     driverctl)
	     	case $cur in
	     	l*)
			COMPREPLY=( "list-devices" "list-overrides" "load-override" )
			return 0
			;;
	     	u*)
			COMPREPLY=( "unset-override" )
			return 0
			;;
	     	s*)
			COMPREPLY=( "set-override" )
			return 0
			;;
		*)
		        COMPREPLY=( --bus= --noprobe --nosave --verbose list-devices list-overrides load-override set-override unset-override )
			return 0
	     		;;
		esac
		;;
	     *)
	        echo -en "<driver>\b\b\b\b\b\b\b\b"
		return 0
		;;
	esac

}
complete -F _driverctl driverctl
